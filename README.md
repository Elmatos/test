## MUP II Zusammenfassung

## C

#### Shell Command:

- `gcc -std=c11 -pedantic -Wall -Wextra -Werror -o helloworld helloworld.c`

#### Execute: `./helloworld`

#### Imports:

- <stdio.h>
- <stdlib.h> (EXIT_SUCCESS / EXIT_FAILURE)
- <math.h> (add: gcc -lm)
- <stdbool.h> (boolean)
- <string.h> (string functions)
- <float.h>
- <time.h>

#### Most important functions:

- `printf("some text and maybe variables %d", var)`
- `scanf("%d", &var)`
- `exit(EXIT_SUCCESS)`
- `sizeOf(var)` --> returns size of variable
- `malloc(sizeOf(var) * numberOfElements)`
- `calloc(numberOfElements, sizeOf(var))`
- `free(var)`
- [`qsort(pointerToArray, arrayLength, elementSize, compareFunc)`]
- `fopen(*filename, *mode)`
- `fclose(FILE *stream)`
- `fgetc(FILE *f)`

#### printf data types:

- %hd --> short int
- %hu --> unsigned short int
- %u --> unsigned int
- **%d --> int**
- %ld --> long int
- %lu --> unsigned long int
- %lld --> long long int
- %llu --> unsigned long long int
- **%c --> char**
- **%s --> string**
- **%f --> float**
- **%lf --> double**
- Lf --> long double
- **%p --> pointer**

#### Preprocessor:

- `#include`
- `#define`
- `#undef`
- `#error`
- `#ifdef`
- `#if`
- `#elif`
- `#endif`
- `#line`
- `#pragma`

#### Lokale Header-Datei (dateiname.h)

- Anfang meist:
  - `#ifndef DATEINAME_H`
  - `#define DATEINAME_H`
- Der Rest wie Interface, deklaration aller zu benutzenden Funktionen
- Ende `#endif`
- Compilieren von Modulen:
  - dateiname.c hat Funktionalitäten
  - dateiname.h hat header
  - dateinamemain.c ist Hauptprogramm, importiert nur dateiname.h
  - `gcc -o dateinamemain dateiname.c dateinamemain.c`

#### Pointer:

- declaration: `type * name`
- referencing: `&` (storage address)
- dereferencing: `*` (content)
  - in declaration: definition of pointer
  - in expression: dereferencing operator (--> content)
- `NULL` --> pointing to nothing (undefined)
- const
- for array a: a[i] == \*(a + i)
- if function expects pointer:
  - void funcName(*a, *b) {...}
  - --> pass the function the memory address:
    - funcName(&a) (a is Variable) || funcName(a) (a is pointer)

#### enum (Enumeration)

- Assigns Numbers to Names, begins with 0
- Syntax: `enum EnumName { FIRST_VAL, SECOND_VAL, ETC };` (FIRST_VAL = 0, SECOND_VAL = 1, etc)

#### union

- Multiple variables share memory (only used if memory is scarce)
- Syntax: `union UnionName { type name; ... };`

#### struct

- kinda an object, can contain multiple variables with different types
- Syntax: `struct StructName { type name; ... };`
- elements accessed with point operator ( StructName.element )
- possibility to assign an element a fixed storage size (in struct declaration: elementType elementName : bitSize; )

- you can omit the Struct/Union/Enum name and assign it directly to a var
  - `struct/union/enum { ... } var;`

#### typedef

- allows you to assign an abbreviation for the name of a datatype
- `typedef struct { ... } Name;` --- `typedef datatype Name;`

#### Speicherallokation

- Arbeitsspeicher unterteilt in _Stack_ und _Heap_
- Stack:
  - Speichert Rücksprungadresse, Parameterwerte einer Func, Variablen des aktuellen Scope
  - Stack Vars werden automatisch alloziert und freigegeben
  - kann überlaufen _Stackoverflow_ zB zu viel Rekursion
- Heap:
  - Vars müssen explizit allozier & freigegeben werden
  - malloc, calloc, free
  - Bsp: `int *numbers = malloc(sizeOf(int) * laenge)`
  - `free(numbers)`

#### Funktionszeiger

- Deklaration:
  - `returnType (* functionName) (parameters)`
  - Bsp: sei f Funktion (int f(int n) {...}) --> `int (*p)(int) = f`
    - Benutzung p wie f: `int x = p(7) || int x = f(7)`
  - Wenn Funktion pointer erwartet: `double ableitung(double(*pointerFunc)(double), someOtherParameters)`
    - einfach Funktion übergeben: `ableitung(squared, someOtherParameters)`
    - squared: `double squared(double x) {...}`

#### Dateien

- In <stdio.h>
- EOF (End Of File)
- FILE Type
- Functions:
  - `FILE *file = fopen(const char *filename, const char *mode);` (file == NULL if failed)
  - `int fclose(FILE *stream);`
  - `int fgetc(FILE *f)` (returns EOF if end of file is reached)
  - `int fputc(int ch, FILE *f)` (Schreibt Zeichen ch in f)
  - `int fputs(const char *s, FILE *f)` (Schreibt s nach f, returns >= 0 bei Erfolg, EOF bei Fehler)
  - `size_t fread(void *buffer, size_t size, size_t n, FILE *f);` (Liest n Elemente, jedes size Bytes groß, aus f in buffer, returns Anzahl an gelesenen Elementen)
  - `size_t fwrite(void *buffer, size_t size, size_t n, FILE *f);` (Schreibt n Elemente, jedes Größe size, aus buffer in f, returns Anzahl)
  - `long ftell(FILE *f);` (Returns aktuelle Position in Datei)
  - `int fseek(FILE *f, long *pos, int origin);` (Setzt aktuelle Position in datei)
  - `int feof(FILE *f);` (returns 0, wenn man nicht am Dateiende ist)
  - `int ferror(FILE *f);` (returns 0, wenn kein Fehler beim Lesen)
  - `int perror(const char *s);` (gibt s und Fehlermeldung des letzten Befehl aus stderr aus)

#### -> Operator

- lässt sich auf Zeiger aus Structs und Unions anwenden
- Dereferenziert und holt das benannte Element
- Bsp: `typedef struct Liste`
  - `Liste *l = malloc(sizeOf(Liste))`
  - `l -> start = NULL`

##### Makefiles

- Automatisieren von Kompilierung

&nbsp;

&nbsp;

## Common LISP

#### Shell Command

- `clisp fileName.cl`

#### Basics

- **UM ALLES KLAMMERN MACHEN!!!**
- Kommentare mit `;`
- Listen sind in Klammern eingeschlossene Folgen von Einträgen --> Alles sind Listen
- true: `T`, false/NULL: `NIL`
- Funktionsaufrufe: `(FunktionsName Arg1 Arg2 ... ArgN)`
- `quote` oder kurz: `'` markiert Listen als Daten
- `eval` behandelt Datenlisten als Ausdrücke (quasi das Gegenteil --> jedes eval entfernt ein quote)

#### Important Functions

- `print`
- `eval`
- `quote`
- `car` (returns Listenkopf)
- `cdr` (returns Liste ohne Kopf)
- `cons` (neue Liste aus Kopf und Restliste)
- `append` (verbindet 2 oder mehr Listen)
- `list` (erzeugt Liste aus Argumenten)
- `first` (erstes Element aus Liste)
- `second` (zweites Element aus Liste)
- `third` (drittes)
- `map`
- `reduce`

#### Logical Operators

- `atom`
- `not`
- `and`
- `or`
- `= /= < > <= >=`
- `eq` (checkt auf Gleichheit (auch Typ), nur für Atome)
- `equal` (Atome + Listen)

#### Fallunterscheidungen

1. if

   - `(if (Bedingung) T-Fall NIL-Fall)`

2. cond

   - `(cond (Bedingung1 Ausdruck1) (Bedinung2 Ausdruck2) (...))`

#### Eigene Funktionen / Variablen mit Let

1. Funktionen

   - `(defun Name (Parameterliste) (Ausdruck))`
   - `(defun add (a b) (+ a b))`

2. Variablen mit let

   - `(let ((Variable1 Ausdruck1) ... (VariableN AusdruckN)) Ausdruck)`
   - Bildet neuen Gültigkeitsbereich mit lokalen Variablen --> jede Variable bekommt Wert des entsprechenden Ausdrucks
   - `(let ((x (cos 30)) (y (sin 30))) (+ (* x x) (* y y)))`

#### Lambdas (anonyme Funktionen)

- `(lambda (parameter) (ausdruck))`
- `((lambda (x) (* x x)) 3)` (wendet Funktion auf 3 an)

#### Funktionen in Funktionen

- Um Funktion aufzurufen muss man `funcall` verwenden
- Wenn man einer Funktion eine Funktion (außer lambda) übergibt muss man `quote` oder `'` verwenden! (sonst wird versucht die Funktion direkt aufzurufen)

```
(defun ableitung (f x dx))
  (/ (- (funcall f (+ x dx)) (funcall f (- x dx))) 2 dx)
)
(defun square (x) (* x x))
(ableitung 'square 1 0.0001)
(ableitung (lambda (x) (* x x)) 1 0.0001)

```

#### map & reduce

- `(map rückgabetyp Funktion Liste)`
- `(map 'list (lambda (x) (* x x)) L))`
- `(reduce Funktion Liste)`
- `(reduce '+ L)`

&nbsp;

&nbsp;

## SWI PROLOG

#### Basics

- Starten mit`swipl`
- Beenden `halt.`
- Datei laden `consult("myprogram.pl")`
- PUNKTE AM ENDE NIE VERGESSEN

#### Aufbau von Prolog Programmen

- Programm besteht aus mind einer Wissensbasis
- Wissensbasen sind Listen von Klauseln
- Jede Klausel entweder Fakt oder Regel
- Regel: `K :- R.`
  - K: Kopf --> Verbundterm
  - :- Hals, quasi ein "falls"
  - R: Rumpf --> ein oder mehrere Ziele
  - . am Ende
- Fakten haben keinen Hals und Rumpf: `K.`
- Anfragen keinen Kopf und Hals: `R.`

##### Verbundterme

- `F(T1, ..., Tn)`
  - F entweder Kleinbuchstabe gefolgt von Buchstaben, Ziffern und \_
  - oder Folge von Spezialzeichen oder Zeichenkette ("bla bla")
- Wenn kein Term, dann können Klammern weggelassen werden (zB `es_regnet.`)

#### Variablen

- Platzhalter, können jeden Term als Wert annehmen
- Großbuchstabe oder _ gefolgt von Buchstaben/Ziffern/_
- "\_" ist der Name für Variablen, deren Wert nicht interessiert

#### Logische Operatoren

- Konjunktion: `,`
- Disjunktion: `;`
- Negation: `not` (Verwendung mit Klammern: `not(mensch(X)).`)
- False: `fail` (`es_regnet :- fail.`)

- Cut-Operator: ! (verhindert Backtracking)

##### Arithmetik

- Ganz normal gottseidank (x + y etc)
- Funktionen:
  - max(X, Y)
  - abs(X)
  - round(X)
  - sqrt(X)

#### Listen

- [T1, T2, ..., Tn]
- [H|T]
- Operationen:
  - length(Liste, Länge).
  - member(Element, Liste).
  - append(Liste1, Liste2, Liste1_2).
  - reverse(Liste, UmgedrehteListe).
  - sort(Liste, SortierteListe).
  - permutation(Liste, PermutierteListe).

&nbsp;

&nbsp;

## Python3

#### Basics

- `bool`
  - `True || False` (Groß geschrieben!)
  - Falsy Values wie in JS
  - Logische Operatoren: `and or not`
- Typumwandlungen:
  - int(), float(), complex(), str()
- Strings
  - Spezielle Zeichen: `\n \r \t`
- Block ohne Anweisungen: `pass`
- Es gibt kein `switch`
- Funktionen:
  - `def Funktionsname(Parameter, ...):`
  - `return`
  - default Werte: `def someName(a=1, b, c=5):`
  - **lambdas**
    - `lambda Parameter1, Parameter2: Ausruck`

#### Important Functions

- `print("something")`
- `input()`
- `type()` (Identifizierung des Typs)
- `len()`
- String Functions:
  - `s.find()` (erstes Vorkommen)
  - `s.rfind()` (letztes Vorkommen)
  - `s.replace()` (neuer String mit Ersetzung)
- List Functions:
  - `L.pop(int)` (removed Wert an Stelle int)
- `del` (delete)
  - Bsp: `del d["foo"]` (Löscht im Dictionary den Schlüssel "foo" + Wert)
- Dateien:
  - `f = open("dateiname.txt", "mode")`
  - file objekte sind iterierbar (for zeile in f:)
  - `f.close()`
  - `f.write()`
  - `f.read()`
- `isInstance(I, K)` (True, wenn I Instanz von Klasse K oder ihre Unterklasse)
- `isSubclass(U, O)` (True, wenn U Unterklasse von O)

#### Slicing

- Syntax: `[Erstes:Letztes:Schrittweite]`
  - Defaults:
    - Erstes: 0
    - Letztes: len() (Geht nur bis vor den Wert)
    - Schrittweite: 1

#### String Formatieren & print

- `"%s=%d" % ("f", 12)` (--> "f=12")
- `print("a=%d,b=%d" % (f(x,n),g(x,n)))`

#### Listen

- Anlegen mit eckigen Klammern
  - `L = []`
- Konkatenieren mit +
  - `L = L + [val, "val"]`
- `L.pop()`
- `L * 2` (Wiederholt die Liste bzw konkateniert an sich selber)
- `L[-1]` (Letztes Element der Liste)

#### Tupel

- Anlegen mit runden Klammern
  - `(1, 2, 3)`
- Unveränderlich
- Zugriff wie bei Listen mit eckigen Klammern
  - `(1, 2, 3)[0]`

#### Dictionary (wie JS object)

- Anlegen mit geschweiften Klammern
  - `d = {"foo": "bar", 1: 3}`
- Zugriff über Schlüssel
  - `d["foo"]`

#### Fallunterscheidungen mit if

- `if, elif, else`
- Bsp:

```
if x > 0:
  print("Positiv")
elif x == 0:
  print("Null")
else:
  print("Negativ")
```

- Kurzversion: AusdruckTrue **if** Bedingung **else** AusdruckFalse
- in:
  - if something **in** something
  - `if 3 in range(3, 7)`

#### Schleifen

- for:
  - `for element in list:`
  - `for i in [1, 2, 3]:`
  - `for i in range(4):` (i = 0, 1, 2, 3 --> iteriert 4 mal, von 0 bis n-1)
- while:
  - `while Ausdruck:`
  - `while x > 4:`
- continue, break
- `else` nach Schleife wird aufgerufen, falls diese nicht mit `break` abgebrochen wurde
- Über Dictionaries iterieren:
  - `for key, value in dict.items():`

#### Comprehensions

- Erzeugen aus einer Iteration eine Liste, ein Set, oder ein Dictionary
- `{i for i in range(1,10)}`
- `[[1 if i == j else 0 for j in range(1,4)] for i in range(1,4)]`
- `[i for i in range(1,10) if i%2==0]`

#### Klassen

- `class ClassName:`
- Vererbung:
  - `class ClassName(Parent):`
- Functions:
  - `__init__` (Konstruktor)
    - `def __init__(self, parameters):`
    - if has parent: `super().__init__(parentParameters)`
    - `self.parameter = parameter` für alle Parameter in `__init__`
  - `__str__` (Quasi toString, wird von print verwendet)
    - `def __str__(self):`
- Aufruf Klasse: `kitty = Katze(parametersVonInitOhneSelf)`

&nbsp;

&nbsp;

## Zahlensysteme

#### Dezimalsystem

- Summendarstellung, Rechnung etc obvious

#### Binärsystem (Dualsystem)

- Summendarstellung: `10101101 = 1 * 2^7 + 1 * 2^5 + 1 * 2^3 + 1 * 2^2 + 1 * 2^0`
- Rechnen:
  - Addition: `110 + 11 = 1001`
  - Substraktion: `101 - 11 = 10`
  - Multiplikation: `101 * 11 = 1010 + 101 = 1111`
  - Division: `1111 : 101 = 11`
    - `1111 : 101`
    - `1111 - 1010` (`= 101 * 10`) --> `1111 - 1010 = 101`
    - --> `101 : 101 = 1`
    - `10 + 1 = 11`

#### Oktal- & Hexadezimalsystem

- Oktal 0 - 7 (Basis 8)
- Hexadezimal 0 - F (Basis 16)
- Bsp Hexadezimal:
  - Summendarstellung: `539BAD = 5 * 16^5 + 2 * 16^4 + 9 * 16^3 + 11 * 16^2 + 10 * 16^1 + 13 * 16^0`

### Umrechnung zwischen Systemen

#### Dezimal --> Dual

- Vorkommateil:
  - Wiederholt durch 2 Teilen und Rest vermerken:
  - `23 / 2 = 11 REST 1`
  - `11 / 2 = 5 REST 1`
  - `5 / 2 = 2 REST 1`
  - `2 / 2 = 1 REST 0`
  - `1 / 2 = 0 REST 1`
- Nachkommateil:
  - Wiederholt mit 2 multiplizieren und Ziffer vor Komma abschneiden:
  - `0,125 * 2 = 0,25 -> 0`
  - `0,25 * 2 = 0,5 --> 0`
  - `0,5 * 2 = 1 --> 1`
- Ablesen Vorkommateil reverse, Nachkommateil normal:
  - `23,125 = 10111,001`

#### Dual --> Oktal/Hexadezimal

- jede Hexadezimalziffer entspricht 4 Dualziffern
- jede Oktalziffer entspricht 3 Dualziffern
- Dualzahl rückt immer auf:
  - `0000 - 0001 - 0010 - 0011 - 0100 - 0101 - 0110 - 0111 - 1000 etc`
- einfaches hochzählen im jeweiligen System
  - zB Oktal: `0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 10 - 11 - 12 - 13 etc`

### Maschinenzahlen

- Eine Zahl, die im Computer in binärer Form gespeichert wird
- --> Jedes Bit entweder 0 oder 1
- Bitwertigkeit:
  - Little Endian: Niedrigstwertige Bits zuerst
  - Big Endian: Höchstwertige Bits zuerst

#### IEEE 754-2008 Standart für Maschinen-Fließkommazahlen

- Jede Zahl != 0 lässt sich zerlegen in `r = +- M * b^E`
  - `1337 = 1,337 * 10^3`
- Fließkomma-Maschinenzahl mit 8 bits (1 Byte)
  - 1 Vorzeichenbit, 2 Exponentenbits, 6 Mantissenbits
  - Vorzeichen s = 0 (+), = 1 (-)
  - Zahl z = 3.15625 = 11.00101
  - Exponentialschreibweise: 1.100101 \* 2¹
  - 1 vor dem Komma wird nicht gespeichert
  - Wenn M zu lang, auf die nächste passende Zahl runden
    - M = 1.100101, M' = 100101 --> M' = 10011
  - Exponenten (hier = 1) werden mit Bias (C, hier 1) gespeichert: E' = E + C (= 10)
    - Bias C ist 2^k-1 - 1, k Anzahl der Exponentenbits
  - Maschinenzahl mit 8 bits: `0 1 0 1 0 0 1 1` (erste 0 Vorzeichen, 1 0 Exponent + Bias, Rest Mantissenbits)

&nbsp;

## Turing Maschine

- Formales Modell für Berechnungen
- unendliches Band aus **Zellen** (nach links und rechts unendlich), jede Zelle 0, 1 oder leer
- **Lese-Schreibkopf (LSK)**
  - steht immer an einer Zelle
  - befindet sich in einem von einer endlichen Anzahl an Zuständen
  - kann sich um eine Zelle bewegen
- **Übergangsfunktion**
  - In Abhängigkeit des aktuellen Zustands und Inhalt der aktuellen Zelle des LSK
  - Beschreibt
    - In welchen neuen Zustand der LSK geht
    - Was in die Zelle beim LSK geschrieben wird
    - In welche Richtung sich der LSK bewegt
  - Kann von einer Übergangstabelle beschrieben werden
- Das Programmieren einer Turing-Maschine umfasst das Übersetzen eines
  Algorithmus in eine Übergangstabelle

#### VL 14.Mai ab Seite 16 Turing Maschine, Berechenbarkeit, Abstraktion, Strukturierte Programmierung, Exceptions, etcetc
